#!/bin/bash
# add user
#   add system group & user,docker images already include these
groupadd ftpgroup
useradd ftpuser -g ftpgroup -d /home/ftp -s /sbin/nologin
#   add ftp user,this step need execute in docker container & after this will need input password
pure-pw useradd user1 -u ftpuser -g ftpgroup -d /var/www/site1
#   let ftp server setup virtual user data
pure-pw mkdb
#   execute ftp restart in docker container or restart docker container
/etc/init.d/pure-ftpd restart


# use wget to download ftp data,use no passive mode
wget --no-passive-ftp ftp://user1:123@192.168.0.6/123/Wireshark.lnk

#!/bin/bash
if docker ps -a | grep jenkins; then docker rm -f jenkins; fi
if docker images | grep jenkins-docker-maven-node; then docker rmi docker.wencst.com:5000/jenkins-docker-maven-node; fi
docker build -t docker.wencst.com:5000/jenkins-docker-maven-node .


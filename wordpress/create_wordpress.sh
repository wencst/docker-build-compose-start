#!/bin/bash
docker run -d --name mysql1 -e MYSQL_ROOT_PASSWORD=root mysql
docker run --name some-wordpress --link mysql1:mysql -p 80:80 -e WORDPRESS_DB_USER=root -e WORDPRESS_DB_PASSWORD=root -d wordpress
